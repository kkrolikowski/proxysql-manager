FROM php:7.2-apache

RUN curl https://codeload.github.com/smarty-php/smarty/tar.gz/v3.1.33 -o /usr/src/smarty.tar.gz -s

WORKDIR /usr/local/lib/php
RUN tar xzf /usr/src/smarty.tar.gz \
    && mv smarty-3.1.33 Smarty \
    && rm -f /usr/src/smarty.tar.gz

COPY src/ /var/www/html/